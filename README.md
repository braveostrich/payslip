# Run with Maven

* Install Maven (skip it if you have maven installed)
* Add mvn to your path
* type `mvn clean verify -Pcode-reviewer` in your command line
* If everything goes fine you will get an output.csv file. The path of that file will be shown in the console.
> The output file can be found at /Users/your_folder/payslip/target/classes/output.csv

# Run as Jar

* Run mvn assembly:assembly -DdescriptorId=jar-with-dependencies to get a single jar with all dependencies.
* Runjava -cp target/payslip-1.0-SNAPSHOT-jar-with-dependencies.jar au.com.myob.payslip.App src/main/resources/input.csv output.csv