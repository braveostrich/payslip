package au.com.myob.payslip;

import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.runner.RunWith;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static junit.framework.Assert.assertEquals;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.junit.Assume.assumeThat;

@RunWith(Theories.class)
public class IncomeTaxCalculatorTest {

    // https://blogs.oracle.com/jacobc/entry/junit_theories
    @DataPoints
    public static IncomeTaxPair[] incomeTaxPairs(){
        return new IncomeTaxPair[]{
           new IncomeTaxPair("60050", "11063.2500"),
           new IncomeTaxPair("18200", "0.0000"),
           new IncomeTaxPair("37000", "3572.0000"),
           new IncomeTaxPair("80000", "17547.0000"),
           new IncomeTaxPair("180000", "54547.0000"),
        };
    }

    private static BigDecimal getAnnualIncomeTax(int annualSalary){
        return IncomeTaxCalculator.getAnnualIncomeTax(BigDecimal.valueOf(annualSalary));
    }

    @Theory
    public void getIncomeTax(IncomeTaxPair pair){
        BigDecimal annualIncomeTax = IncomeTaxCalculator.getAnnualIncomeTax(new BigDecimal(pair.income));
        assertEquals(new BigDecimal(pair.incomeTax), annualIncomeTax.setScale(4, RoundingMode.HALF_UP));
    }

    private static class IncomeTaxPair {
        private String income;
        private String incomeTax;

        private IncomeTaxPair(String income, String incomeTax) {
            this.income = income;
            this.incomeTax = incomeTax;
        }
    }

    @DataPoints
    public static int[] annualSalaries = {100, 1000, 10000, 100000};
    @Theory
    public void the_more_you_earn_the_more_you_pay(int annualSalary1, int annualSalary2){
        assumeThat(annualSalary1, greaterThan(annualSalary2));
        assertThat(getAnnualIncomeTax(annualSalary1),
                greaterThanOrEqualTo(getAnnualIncomeTax(annualSalary2)));
    }

}
