package au.com.myob.payslip;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.runner.RunWith;

import java.math.BigDecimal;
import java.text.ParseException;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(Theories.class)
public class PayPeriodCalculatorTest {

    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormat.forPattern("dd MMMM");

    @DataPoints
    public static PayPeriodData[] payPeriodDataArray = {
            new PayPeriodData("01 March", "1.00"),
            new PayPeriodData("02 March", "0.95"),
            new PayPeriodData("02 January", "0.96"),
            new PayPeriodData("01 July", "1.00"),
    };

    @Theory
    public void calculate_the_pay_period(PayPeriodData payPeriodData) throws ParseException {

        DateTime startDate = DATE_TIME_FORMATTER.parseDateTime(payPeriodData.startDate);
        int thisYear = new DateTime().getYear();
        startDate = startDate.withYear(thisYear);

        WorkingDaysValidator workingDaysValidator = new WorkingDaysValidator();
        PayPeriod payPeriod = new PayPeriod(startDate, workingDaysValidator);
        assertThat(payPeriod.getUtilizationRate(), is(new BigDecimal(payPeriodData.utilizationRate)));

    }

    private static class PayPeriodData {
        private String startDate;
        private String utilizationRate;

        private PayPeriodData(String startDate, String utilizationRate) {
            this.startDate = startDate;
            this.utilizationRate = utilizationRate;
        }
    }
}
