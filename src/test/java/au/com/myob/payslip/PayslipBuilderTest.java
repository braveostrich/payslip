package au.com.myob.payslip;

import org.junit.Test;

import java.text.ParseException;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class PayslipBuilderTest {

    @Test
    public void build_payslip_from_input_provider() throws ParseException {
        PayslipBuilder payslipBuilder = new PayslipBuilder();
        payslipBuilder.with("David", "Rudd", "60050", "9%", "01 March");
        payslipBuilder.with("Ryan", "Chen", "120000", "10%", "02 March");

        Payslip payslip = payslipBuilder.build();
        assertThat(payslip.get(0).toString(), is("David Rudd,01 March - 31 March,5004,922,4082,450\n"));
        assertThat(payslip.get(1).toString(), is("Ryan Chen,02 March - 31 March,9500,2561,6939,950\n"));
    }

    @Test
    public void output_payslip_to_advanced_format() throws ParseException {
        PayslipBuilder payslipBuilder = new PayslipBuilder();
        payslipBuilder.with("David", "Rudd", "60050", "9%", "01 March");
        payslipBuilder.with("Ryan", "Chen", "120000", "10%", "02 March");

        Payslip payslip = payslipBuilder.build();

        PayslipHtmlTableFormatter payslipFormatter = new PayslipHtmlTableFormatter();
        payslip.print(payslipFormatter);
        assertThat(payslipFormatter.toString(), is("<table>\n" +
                "<tr><th>Name</th><th>Net Income</th></tr>\n" +
                "<tr><td>David Rudd</td><td>4082</td></tr>\n" +
                "<tr><td>Ryan Chen</td><td>6939</td></tr>\n" +
                "</table>"));
        System.out.println(payslipFormatter.toString());

        /**
         * <table>
         *     <tr>
         *         <th>Name</th><th>Net Income</th>
         *     </tr>
         *     <tr>
         *         <td>David Rudd</td><td>4246</td>
         *     </tr>
         *     <tr>
         *         <td>David Rudd</td><td>4246</td>
         *     </tr>
         * </table>
         */
    }
}
