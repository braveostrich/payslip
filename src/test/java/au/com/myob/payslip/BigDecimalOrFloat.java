package au.com.myob.payslip;

import org.junit.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.NumberFormat;
import java.text.ParseException;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.fail;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class BigDecimalOrFloat {

    @Test
    public void float_is_a_bad_choice_for_calculate_money() {
        // http://www.javaranch.com/journal/2003/07/MoneyInJava.html
        // http://javarevisited.blogspot.com.au/2012/02/java-mistake-1-using-float-and-double.html
        float a = 8250325.12f;
        float b = 4321456.31f;
        float c = a + b;
        assertEquals("$12,571,782.00",NumberFormat.getCurrencyInstance().format(c));
    }

    @Test
    public void big_decimal_saves_us() {
        BigDecimal a1 = new BigDecimal("8250325.12");
        BigDecimal b1 = new BigDecimal("4321456.31");
        BigDecimal c1 = a1.add(b1);
        assertEquals("$12,571,781.43",NumberFormat.getCurrencyInstance().format(c1));
    }

    @Test
    public void big_decimal_rounding() {
        assertEquals(8250325, new BigDecimal("8250325.49").setScale(0, RoundingMode.HALF_UP).intValueExact());
        assertEquals(8250326, new BigDecimal("8250325.50").setScale(0, RoundingMode.HALF_UP).intValueExact());
        assertEquals(8250326, new BigDecimal("8250325.51").setScale(0, RoundingMode.HALF_UP).intValueExact());
    }

    // http://stackoverflow.com/questions/2749375/arithmeticexception-thrown-during-bigdecimal-divide
    @Test(expected = ArithmeticException.class)
    public void exception_thrown_when_rounding_mode_not_provided(){
        BigDecimal a = new BigDecimal("1.6");
        BigDecimal b = new BigDecimal("9.2");
        //noinspection ResultOfMethodCallIgnored
        a.divide(b); // results in the following exception.
        fail("The above line of code should throw an exception.");
    }

    @Test
    public void test_percentage_format() throws ParseException {
        NumberFormat numberFormat = NumberFormat.getPercentInstance();
        assertThat(numberFormat.format(new BigDecimal("1.00")), is("100%"));
        assertThat(numberFormat.format(new BigDecimal("0.09")), is("9%"));
        System.out.println(new BigDecimal(numberFormat.parse("100%").doubleValue()));
        System.out.println(numberFormat.parse("100%").getClass());
//        assertThat(numberFormat.parse("100%"), is(new BigDecimal("1")));
        System.out.println(numberFormat.format(1.00));
        System.out.println(numberFormat.format(new BigDecimal("1.00")));
    }


}
