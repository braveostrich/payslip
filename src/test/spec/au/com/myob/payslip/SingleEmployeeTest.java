package au.com.myob.payslip;

import org.concordion.integration.junit4.ConcordionRunner;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.runner.RunWith;

import java.text.NumberFormat;
import java.text.ParseException;

@RunWith(ConcordionRunner.class)
public class SingleEmployeeTest {
    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormat.forPattern("dd MMMM");

    public AnnualSalary annualSalary;
    private PayPeriod payPeriod;
    private Superannuation superannuation;

    public void setAnnualSalary(String annualSalary) {
        this.annualSalary = new AnnualSalary(annualSalary);
    }

    public void setSuperRate(String superRate) throws ParseException {
        superannuation = new Superannuation(superRate, NumberFormat.getPercentInstance());
    }

    public void setMonth(String month) {
        DateTime startDate = DATE_TIME_FORMATTER.parseDateTime(month);
        int thisYear = new DateTime().getYear();
        startDate = startDate.withYear(thisYear);

        WorkingDaysValidator workingDaysValidator = new WorkingDaysValidator();
        payPeriod = new PayPeriod(startDate, workingDaysValidator);
    }

    public String getPayPeriod() {
        return String.format("%s - %s", DATE_TIME_FORMATTER.print(payPeriod.getStartDate()), DATE_TIME_FORMATTER.print(payPeriod.getEndDate()));
    }

    public int getGrossIncome() {
        return annualSalary.getMonthlyGrossIncome(payPeriod.getUtilizationRate());
    }

    public int getIncomeTax() {
        return annualSalary.getMonthlyIncomeTax(payPeriod.getUtilizationRate());
    }

    public int getNetIncome() {
        return getGrossIncome() - getIncomeTax();
    }

    public int getSuper() {
        return superannuation.getAmount(getGrossIncome());
    }
}
