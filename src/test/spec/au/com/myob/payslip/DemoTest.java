package au.com.myob.payslip;

import org.concordion.integration.junit4.ConcordionRunner;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.runner.RunWith;

import java.text.NumberFormat;
import java.text.ParseException;

@RunWith(ConcordionRunner.class)
public class DemoTest {
    private static final DateTimeFormatter DATE_TIME_FORMATTER_INPUT = DateTimeFormat.forPattern("dd MMMM");
    private final WorkingDaysValidator workingDaysValidator = new WorkingDaysValidator();
    private AnnualSalary annualSalary;
    private PayPeriod payPeriod;
    private Superannuation superannuation;

    public PayPeriod getPayPeriod(String startDateString){
        DateTime startDate = getStartDate(startDateString);

        return new PayPeriod(startDate, workingDaysValidator);
    }

    private DateTime getStartDate(String startDateString) {
        DateTime startDate = DATE_TIME_FORMATTER_INPUT.parseDateTime(startDateString);
        int thisYear = new DateTime().getYear();
        startDate = startDate.withYear(thisYear);
        return startDate;
    }

    public int getWorkingDays(PayPeriod payPeriod){
        DateTime startDate = payPeriod.getStartDate();
        DateTime firstDayOfMonth = startDate.withDayOfMonth(startDate.dayOfMonth().getMinimumValue());

        return workingDaysValidator.getWorkingDays(firstDayOfMonth, payPeriod.getEndDate());
    }

    public int getYourWorkingDays(PayPeriod payPeriod){
        return workingDaysValidator.getWorkingDays(payPeriod.getStartDate(), payPeriod.getEndDate());
    }

    public String getUtilizationRate(PayPeriod payPeriod) {
        NumberFormat numberFormat = NumberFormat.getPercentInstance();
        return numberFormat.format(payPeriod.getUtilizationRate());
    }

    // ======================Second Example======================
    public void initializeAnnualSalarySuperRateAndPayPeriod(String annualSalaryString, String superRate, String startDateString)
            throws ParseException {
        annualSalary = new AnnualSalary(annualSalaryString);
        payPeriod = new PayPeriod(getStartDate(startDateString), workingDaysValidator);
        superannuation = new Superannuation(superRate, NumberFormat.getPercentInstance());
    }

    public int getGrossIncome(){
        return annualSalary.getMonthlyGrossIncome(payPeriod.getUtilizationRate());
    }

    public int getIncomeTax(){
        return annualSalary.getMonthlyIncomeTax(payPeriod.getUtilizationRate());
    }

    public int getNetIncome(){
        return annualSalary.getMonthlyGrossIncome(payPeriod.getUtilizationRate())
                - annualSalary.getMonthlyIncomeTax(payPeriod.getUtilizationRate());
    }

    public int getSuper(){
        return superannuation.getAmount(annualSalary.getMonthlyGrossIncome(payPeriod.getUtilizationRate()));
    }
}
