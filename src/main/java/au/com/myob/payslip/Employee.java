package au.com.myob.payslip;

import org.joda.time.DateTime;

import java.math.BigDecimal;

public class Employee {
    private final FullName name;
    private final AnnualSalary annualSalary;
    private final Superannuation superannuation;

    // TODO: Design concern - Inject it via constructor?
    private final WorkingDaysValidator workingDaysValidator = new WorkingDaysValidator();

    public Employee(FullName name, AnnualSalary annualSalary, Superannuation superannuation) {
        this.name = name;
        this.superannuation = superannuation;
        this.annualSalary = annualSalary;
    }

    // TODO: Design concern - why not FullName.print(PayslipFormatter) and AnnualSalary.print(PayslipFormatter)
    public void print(PayslipFormatter payslipFormatter, DateTime startDate){
        payslipFormatter.beginItem();

        printName(payslipFormatter);

        PayPeriod payPeriod = new PayPeriod(startDate, workingDaysValidator);
        printPayPeriod(payslipFormatter, payPeriod.getStartDate(), payPeriod.getEndDate());
        printIncomeAndSuper(payslipFormatter, payPeriod.getUtilizationRate());

        payslipFormatter.endItem();
    }

    private void printName(PayslipFormatter payslipFormatter) {
        payslipFormatter.withFirstName(name.getFirstName());
        payslipFormatter.withLastName(name.getLastName());
    }

    private void printPayPeriod(PayslipFormatter payslipFormatter, DateTime startDate, DateTime endDate) {
        payslipFormatter.withStartDate(startDate);
        payslipFormatter.withEndDate(endDate);
    }

    private void printIncomeAndSuper(PayslipFormatter payslipFormatter, BigDecimal utilizationRate) {
        int grossIncome = annualSalary.getMonthlyGrossIncome(utilizationRate);
        int incomeTax = annualSalary.getMonthlyIncomeTax(utilizationRate);
        payslipFormatter.withGrossIncome(grossIncome);
        payslipFormatter.withIncomeTax(incomeTax);
        payslipFormatter.withNetIncome(grossIncome - incomeTax);
        payslipFormatter.withSuper(superannuation.getAmount(grossIncome));
    }

}
