package au.com.myob.payslip;

import java.math.BigDecimal;

public abstract class IncomeTaxCalculator {

    private static final BigDecimal INCOME_TAX_RATE_1 = new BigDecimal("0.19");
    private static final BigDecimal INCOME_TAX_RATE_2 = new BigDecimal("0.325");
    private static final BigDecimal INCOME_TAX_RATE_3 = new BigDecimal("0.37");
    private static final BigDecimal INCOME_TAX_RATE_4 = new BigDecimal("0.45");

    private static final BigDecimal INCOME_TAX_RATE_INCREASED_2 = INCOME_TAX_RATE_2.subtract(INCOME_TAX_RATE_1);
    private static final BigDecimal INCOME_TAX_RATE_INCREASED_3 = INCOME_TAX_RATE_3.subtract(INCOME_TAX_RATE_2);
    private static final BigDecimal INCOME_TAX_RATE_INCREASED_4 = INCOME_TAX_RATE_4.subtract(INCOME_TAX_RATE_3);

    private static final BigDecimal TAXABLE_INCOME_THRESHOLD_4 = BigDecimal.valueOf(180000);
    private static final BigDecimal TAXABLE_INCOME_THRESHOLD_3 = BigDecimal.valueOf(80000);
    private static final BigDecimal TAXABLE_INCOME_THRESHOLD_2 = BigDecimal.valueOf(37000);
    private static final BigDecimal TAXABLE_INCOME_THRESHOLD_1 = BigDecimal.valueOf(18200);

    public static BigDecimal getAnnualIncomeTax(BigDecimal annualSalary) {
        BigDecimal level1 = getIncreasedIncomeTax(annualSalary, TAXABLE_INCOME_THRESHOLD_1, INCOME_TAX_RATE_1);
        BigDecimal level2 = getIncreasedIncomeTax(annualSalary, TAXABLE_INCOME_THRESHOLD_2, INCOME_TAX_RATE_INCREASED_2);
        BigDecimal level3 = getIncreasedIncomeTax(annualSalary, TAXABLE_INCOME_THRESHOLD_3, INCOME_TAX_RATE_INCREASED_3);
        BigDecimal level4 = getIncreasedIncomeTax(annualSalary, TAXABLE_INCOME_THRESHOLD_4, INCOME_TAX_RATE_INCREASED_4);
        return level1.add(level2).add(level3).add(level4);
    }

    private static BigDecimal getIncreasedIncomeTax(BigDecimal annualSalary, BigDecimal taxableIncomeThreshold, BigDecimal incomeTaxRate) {
        return annualSalary.subtract(taxableIncomeThreshold).max(BigDecimal.ZERO).multiply(incomeTaxRate);
    }

}
