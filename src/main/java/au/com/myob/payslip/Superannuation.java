package au.com.myob.payslip;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.NumberFormat;
import java.text.ParseException;

public class Superannuation {

    private final BigDecimal superRate;

    public Superannuation(String superRate, NumberFormat numberFormat) throws ParseException {
        Number superRateNumber = numberFormat.parse(superRate);
        this.superRate = new BigDecimal(superRateNumber.doubleValue());
    }

    public int getAmount(int base){
        return BigDecimal.valueOf(base)
                .multiply(superRate)
                .setScale(0, RoundingMode.HALF_UP)
                .intValue();
    }
}
