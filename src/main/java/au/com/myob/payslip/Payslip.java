package au.com.myob.payslip;

import java.util.ArrayList;
import java.util.List;

public class Payslip {
    private List<PayslipItem> payslipItems = new ArrayList<PayslipItem>();

    public void add(PayslipItem payslipItem) {
        payslipItems.add(payslipItem);
    }

    public PayslipItem get(int i) {
        return payslipItems.get(i);
    }

    public void print(PayslipFormatter payslipFormatter) {
        payslipFormatter.begin();
        for(PayslipItem payslipItem : payslipItems){
            payslipItem.print(payslipFormatter);
        }
        payslipFormatter.end();
    }

}
