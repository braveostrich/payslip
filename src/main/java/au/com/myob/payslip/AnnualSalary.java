package au.com.myob.payslip;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class AnnualSalary {
    private static final BigDecimal TWELVE_MONTHS = BigDecimal.valueOf(12);

    private BigDecimal annualSalary;
    private BigDecimal annualIncomeTax;

    // TODO: accept BigDecimal?
    public AnnualSalary(String annualSalary) {
        this.annualSalary = new BigDecimal(annualSalary);
        this.annualIncomeTax = IncomeTaxCalculator
                .getAnnualIncomeTax(this.annualSalary);
    }

    public int getMonthlyGrossIncome(BigDecimal utilizationRate) {
        return toMonthly(annualSalary, utilizationRate);
    }

    public int getMonthlyIncomeTax(BigDecimal utilizationRate) {
        return toMonthly(annualIncomeTax, utilizationRate);
    }

    private int toMonthly(BigDecimal annualAmount, BigDecimal utilizationRate) {
        return annualAmount
                .divide(TWELVE_MONTHS, 0, RoundingMode.HALF_UP)
                .multiply(utilizationRate)
                .intValue();
    }
}
