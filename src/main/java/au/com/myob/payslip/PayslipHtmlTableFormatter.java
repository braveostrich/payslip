package au.com.myob.payslip;

import org.joda.time.DateTime;

public class PayslipHtmlTableFormatter implements PayslipFormatter {
    private StringBuilder result = new StringBuilder();

    @Override
    public void begin() {
        result.append("<table>\n" +
                "<tr>" +
                "<th>Name</th><th>Net Income</th>" +
                "</tr>\n");
    }

    @Override
    public void end() {
        result.append("</table>");
    }

    @Override
    public void beginItem() {
        result.append("<tr>");

    }

    @Override
    public void withFirstName(String firstName) {
        result.append("<td>");
        result.append(firstName);
    }

    @Override
    public void withLastName(String lastName) {
        result.append(" ");
        result.append(lastName);
        result.append("</td>");
    }

    @Override
    public void withStartDate(DateTime startDate) {

    }

    @Override
    public void withEndDate(DateTime endDate) {

    }

    @Override
    public void withGrossIncome(int grossIncome) {

    }

    @Override
    public void withIncomeTax(int incomeTax) {

    }

    @Override
    public void withNetIncome(int netIncome) {
        result.append("<td>");
        result.append(netIncome);
        result.append("</td>");
    }

    @Override
    public void withSuper(int superAmount) {

    }

    @Override
    public void endItem() {
        result.append("</tr>\n");
    }

    @Override
    public String toString(){
        return result.toString();
    }
}
