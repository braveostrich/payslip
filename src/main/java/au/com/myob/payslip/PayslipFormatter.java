package au.com.myob.payslip;

import org.joda.time.DateTime;

public interface PayslipFormatter {
    void begin();

    void end();

    void beginItem();

    void withFirstName(String firstName);

    void withLastName(String lastName);

    void withStartDate(DateTime startDate);

    void withEndDate(DateTime endDate);

    void withGrossIncome(int grossIncome);

    void withIncomeTax(int incomeTax);

    void withNetIncome(int netIncome);

    void withSuper(int superAmount);

    void endItem();
}
