package au.com.myob.payslip;

import org.joda.time.DateTime;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class PayPeriod {
    private final DateTime startDate;
    private final DateTime endDate;
    private final WorkingDaysValidator workingDaysValidator;

    public PayPeriod(DateTime startDate, WorkingDaysValidator workingDaysValidator) {
        this.startDate = startDate;
        this.endDate = getEndDate(startDate);
        this.workingDaysValidator = workingDaysValidator;
    }

    public BigDecimal getUtilizationRate() {
        int yourWorkingDays = workingDaysValidator.getWorkingDays(startDate, endDate);

        DateTime firstDayOfMonth = startDate.withDayOfMonth(startDate.dayOfMonth().getMinimumValue());
        int workingDaysOfThisMonth = workingDaysValidator.getWorkingDays(firstDayOfMonth, endDate);

        return BigDecimal.valueOf(yourWorkingDays).divide(BigDecimal.valueOf(workingDaysOfThisMonth), 2, RoundingMode.HALF_UP);
    }

    public DateTime getStartDate() {
        return startDate;
    }

    public DateTime getEndDate() {
        return endDate;
    }

    private static DateTime getEndDate(DateTime startDateThisYear) {
        int lastDayOfThisMonth = startDateThisYear.dayOfMonth().getMaximumValue();
        return startDateThisYear.withDayOfMonth(lastDayOfThisMonth);
    }
}
