package au.com.myob.payslip;

import org.joda.time.DateTime;

public class PayslipItem {
    private final Employee employee;
    private final DateTime startDate;

    public PayslipItem(Employee employee, DateTime startDate){
        this.employee = employee;
        this.startDate = startDate;
    }

    @Override
    public String toString(){
        PayslipCSVFormatter payslipFormatter = new PayslipCSVFormatter();
        print(payslipFormatter);
        return payslipFormatter.toString();
    }

    public void print(PayslipFormatter payslipFormatter) {
        employee.print(payslipFormatter, startDate);
    }
}
