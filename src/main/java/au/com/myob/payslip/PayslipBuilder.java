package au.com.myob.payslip;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

public class PayslipBuilder {
    // Formatter should occur where the raw string is given
    private static final DateTimeFormatter DATE_TIME_FORMATTER_INPUT = DateTimeFormat.forPattern("dd MMMM");
    private final List<PaymentInfo> paymentInfoArrayList = new ArrayList<PaymentInfo>();

    public void with(String firstName, String lastName, String annualSalary, String superRate, String paymentStartDate) {
        paymentInfoArrayList.add(new PaymentInfo(firstName, lastName, annualSalary, superRate, paymentStartDate));
    }

    public Payslip build() throws ParseException {
        Payslip payslip = new Payslip();

        for (PaymentInfo paymentInfo : paymentInfoArrayList) {
            Employee employee = getEmployee(paymentInfo);
            DateTime startDate = getStartDate(paymentInfo);
            payslip.add(new PayslipItem(employee, startDate));
        }
        return payslip;
    }

    private Employee getEmployee(PaymentInfo paymentInfo) throws ParseException {
        FullName name = new FullName(paymentInfo.firstName, paymentInfo.lastName);
        AnnualSalary annualSalary = new AnnualSalary(paymentInfo.annualSalary);
        Superannuation superannuation = new Superannuation(paymentInfo.superRate, NumberFormat.getPercentInstance());
        return new Employee(name, annualSalary, superannuation);
    }

    private DateTime getStartDate(PaymentInfo paymentInfo) {
        DateTime startDate = DATE_TIME_FORMATTER_INPUT.parseDateTime(paymentInfo.startDate);
        int thisYear = new DateTime().getYear();
        return startDate.withYear(thisYear);
    }

    private class PaymentInfo {
        private String firstName;
        private String lastName;
        private String annualSalary;
        private String startDate;
        private String superRate;

        public PaymentInfo(String firstName, String lastName, String annualSalary, String superRate, String paymentStartDate) {
            this.firstName = firstName;
            this.lastName = lastName;
            this.annualSalary = annualSalary;
            this.startDate = paymentStartDate;
            this.superRate = superRate;
        }
    }
}
