package au.com.myob.payslip;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class PayslipCSVFormatter implements PayslipFormatter {
    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormat.forPattern("dd MMMM");

    private final StringBuilder result = new StringBuilder();
    private String firstName;
    private String lastName;
    private DateTime startDate;
    private DateTime endDate;
    private int grossIncome;
    private int incomeTax;
    private int netIncome;
    private int superAmount;

    @Override
    public void begin() {

    }

    @Override
    public void end() {

    }

    @Override
    public void beginItem() {

    }

    @Override
    public void withFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Override
    public void withLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public void withStartDate(DateTime startDate) {
        this.startDate = startDate;
    }

    @Override
    public void withEndDate(DateTime endDate) {
        this.endDate = endDate;
    }

    @Override
    public void withGrossIncome(int grossIncome) {
        this.grossIncome = grossIncome;
    }

    @Override
    public void withIncomeTax(int incomeTax) {
        this.incomeTax = incomeTax;
    }

    @Override
    public void withNetIncome(int netIncome) {
        this.netIncome = netIncome;
    }

    @Override
    public void withSuper(int superAmount) {
        this.superAmount = superAmount;
    }

    @Override
    public void endItem() {
        String row = String.format("%s %s,%s - %s,%d,%d,%d,%d",
                firstName, lastName,
                DATE_TIME_FORMATTER.print(startDate), DATE_TIME_FORMATTER.print(endDate),
                grossIncome, incomeTax, netIncome, superAmount);
        result.append(row);
        result.append("\n");
    }

    @Override
    public String toString(){
        return result.toString();
    }
}
