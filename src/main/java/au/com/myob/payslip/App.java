package au.com.myob.payslip;

import com.google.common.base.Charsets;
import com.google.common.base.Splitter;
import com.google.common.io.Files;
import com.google.common.io.LineProcessor;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.Iterator;

public class App {
    public static void main(String[] args) throws IOException {
        if (args.length < 2){
            System.out.println("Usage: java -cp payslip-1.0-SNAPSHOT-jar-with-dependencies.jar au.com.myob.payslip.App input.csv output.csv");
            return;
        }
        final PayslipBuilder payslipBuilder = new PayslipBuilder();

        File inputFile = new File(args[0]);
        Payslip payslip = (Payslip) Files.readLines(inputFile, Charsets.UTF_8, new LineProcessor<Object>() {
            @Override
            public boolean processLine(String line) throws IOException {
                buildPayslipForLine(line);
                return true;
            }

            // TODO: Exception handling
            private void buildPayslipForLine(String line) {
                Iterable<String> split = Splitter.on(",").split(line);
                Iterator<String> cell = split.iterator();
                String firstName = cell.next();
                String lastName = cell.next();
                String annualSalary = cell.next();
                String superRate = cell.next();
                String startDate = cell.next();
                payslipBuilder.with(firstName, lastName, annualSalary, superRate, startDate);
            }

            @Override
            public Object getResult() {
                try {
                    return payslipBuilder.build();
                } catch (ParseException e) {
                    return null;
                }
            }
        });

        PayslipFormatter payslipFormatter = new PayslipCSVFormatter();
        payslip.print(payslipFormatter);

        Files.write(payslipFormatter.toString(), new File(args[1]), Charsets.UTF_8);

        System.out.println(String.format("The output file is stored at %s", args[1]));
    }
}
