package au.com.myob.payslip;

import org.joda.time.DateTime;

public class WorkingDaysValidator {

    private static final int FRIDAY_DAY_OF_WEEK = 5;

    public int getWorkingDays(DateTime from, DateTime to) {
        int workingDays = 0;
        for(; !from.isAfter(to); from = from.plusDays(1)){
            if(isWorkingDay(from)){
                workingDays++;
            }
        }
        return workingDays;
    }

    // Assume all Monday ~ Friday are working days.
    private boolean isWorkingDay(DateTime dateTime) {
        return dateTime.getDayOfWeek() <= FRIDAY_DAY_OF_WEEK;
    }
}
